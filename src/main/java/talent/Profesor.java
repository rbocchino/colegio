package talent;

import talent.enums.Sexo;

public class Profesor extends Persona {

	public Profesor(String _nombre, String _apellidos, String _dni, int _edad, Sexo _sexo) {
		super(_nombre, _apellidos, _dni, _edad, _sexo, 5);
	}

}
