package talent;

import java.util.Scanner;

import talent.exception.DatoVacioException;

public class Utils {
	private static Scanner sc = new Scanner(System.in);
	
	private Utils() {}
	
	public static int pedirEnteroPorPantalla() throws NumberFormatException, DatoVacioException {
		String valor = Utils.pedirStringPorPantalla();
		return Integer.parseInt(valor);
	}
	
	public static String pedirStringPorPantalla() throws DatoVacioException {
		String dato = sc.nextLine();
		if (Utils.esCadenaVaciaNula(dato)) {
			throw new DatoVacioException();
		}
		return dato;
	}
	
	public static boolean esCadenaVaciaNula(String cadena) {
		return cadena == null || cadena.trim().isEmpty();
	}
}
